<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+T6KrjAwgY7mTF7geKdj7mZgAdlvrIJwqqg+pj5YL+7rVdnu6aHLQZYmn8/mjBh1wDh0cF82qScRtfJ7kdxiZA==');
define('SECURE_AUTH_KEY',  'oxaXohQ0W2J12tfMl5+w8KoI5FQD1HbN/baA5kNsuQMnMnGk/wLeaqaMZNkTnjRG/TzV063VG5wqMJHmZbBH2w==');
define('LOGGED_IN_KEY',    'vg20S1vAv2ootKmTV3W6kRzGSJQT/ZWHt4L+tT0sVFs0JpEb5jinZNn34jn49MKh6IQ9F6wMirdI0WHK4FbZAw==');
define('NONCE_KEY',        'SOCF1lKM+eb4hGUpIVcr8vCIhjWSKaY8kfz9ckxUN72LczstlpQxMAJYnAd/40qSmRbjyWv86PCpWZqlOarBKA==');
define('AUTH_SALT',        '/Sx6U/7O5OrVSo9scnEwzPgMHSi4afHBEBfuhmkQtgRV4UTlPafRzp2kQNmoAfR2dIzVSTP2oiSJAHQFYGWlcg==');
define('SECURE_AUTH_SALT', 'RuijSEKrcQx48q0H7CECY8b9fJF9waQOm+XpKZKnRxpo95W9AsxynKPrQM69Vhbx+mYmSxzqbW8Zg7ldN5EH/Q==');
define('LOGGED_IN_SALT',   'lSO8/kTA/YrAnrYuSbISCqgdmgrPNJrD4mbUZqKOzsvozNemYdXpzYotQRk5uFvBmBjtv7RnCRHyfjw2Y48GPw==');
define('NONCE_SALT',       'bpw6y4bKy5RDw4JSDs++eLJULQoPp0fggXk1INa7nAnGP8dv9CJPxK6TwYfK6PT+Vy5T+Ql3oDs3HEw12nX49w==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
